myFirstListToday = ["name1", "name2", "name3"]

myFirstListToday.insert(0,"Jeffe")

del(myFirstListToday[2])

print(myFirstListToday[2])

for name in myFirstListToday:
    print ("Name was: {} at index: {}".format(name, myFirstListToday.index(name)))

dictionary = {
    "Alphabet": "Letters and stuff",
    "Bear": "Fluffy"
}

for word in dictionary:
    print ("The word '{}' means {}".format(word, dictionary[word]))

class Dog:

    def __init__(self, name, age, breed):
        self.name = name
        self.age = age
        self.breed = breed

    def print_details(self):
        print("{} is a {} that is {} years old".format(self.name, self.breed, self.age))


dog = Dog("Terence", 1, "zoomie boi")
dog.print_details()