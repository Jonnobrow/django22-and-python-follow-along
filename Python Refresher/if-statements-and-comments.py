age = 16
name = "Jonathan"

if age > 18:
    print("Hi {} you are an adult".format(name))
else:
    print("Too young, come back in {} years".format(18-age))