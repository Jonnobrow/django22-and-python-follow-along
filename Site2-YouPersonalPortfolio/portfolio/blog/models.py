from django.db import models

# Create your models here.
class BlogPost(models.Model):
    title=models.CharField(max_length=127)
    content=models.CharField(max_length=10000)
    pub_date=models.DateField()
    image=models.ImageField(upload_to='images/')