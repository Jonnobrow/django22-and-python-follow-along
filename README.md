# Django 2.2 & Python - Follow Along

Following along to the Udemy Course: https://www.udemy.com/the-ultimate-beginners-guide-to-django-django-2-python-web-dev-website/ as part of training in my summer placement 2019

## Sections
1. [Python Refresher](#python-refresher)
2. [Site #1](#site-1)
3. [Site #2](#site-2)
4. [VPS](#vps)
5. [Site #3](#site-3)

## Python Refresher

This section was just some simple recap stuff for Python. Very low level.

You can find the code files [here](Python-Refresher)

## Site #1 

## Site #2

## VPS

## Site #3
