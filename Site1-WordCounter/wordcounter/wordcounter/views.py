"""
Imports
"""
from django.http import HttpResponse
from django.shortcuts import render
import operator

"""
The home page for word counter
"""
def home(request):
    return render(request, "home.html")

def count(request):
    text = request.GET['full-text']
    count = len(text.split(" "))
    wordcountdict = {}

    for word in text.split(" "):
        if word in wordcountdict:
            wordcountdict[word] += 1
        else:
            wordcountdict[word] = 1

    return render(request, "count.html", {"wordcount": count, "wordcountdict": sorted(wordcountdict.items(), key=operator.itemgetter(1), reverse=True)})