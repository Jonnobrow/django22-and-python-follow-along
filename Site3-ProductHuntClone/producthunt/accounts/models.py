from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Product(models.Model):

    title       =   models.CharField(max_length=255)
    url         =   models.URLField()
    icon        =   models.ImageField(upload_to='product_icons')
    image       =   models.ImageField(upload_to='product_images')
    pub_date    =   models.DateField()
    votes_total =   models.IntegerField()
    body        =   models.TextField()
    hunter      =   models.ForeignKey(User, on_delete=models.CASCADE, default=None, null=True)

    def pub_date_pretty(self):
        return self.pub_date.strftime("%b %e %Y")
