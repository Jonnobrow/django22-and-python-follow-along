# Generated by Django 2.2.4 on 2019-08-23 11:12

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255)),
                ('url', models.URLField()),
                ('icon', models.ImageField(upload_to='product_icons')),
                ('image', models.ImageField(upload_to='product_images')),
                ('pub_date', models.DateField()),
                ('votes_total', models.IntegerField()),
                ('body', models.TextField()),
            ],
        ),
    ]
